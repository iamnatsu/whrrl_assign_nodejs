const db = require('../db');

exports.signin = (req,res) =>{

    const {username,password} = req.body;
    if(username == '' || password==''){
        res.jsonp({
            success:false,
            message:"Username and Password should not be empty"
        })
    }else{
        var sql_select = "select * from user where username=(?)";

        db.all(sql_select,[username], (err,rows) =>{
            if (err) {
                console.log("Error \n", err);
                res.jsonp({
                    success:false,
                    error:err
                })
              }else{
                  if(rows.length > 0){
                    res.jsonp({
                        success:false,
                        message:'Username already exits'
                    })
                  }else{
                     
                     var sql_insert = "insert into user(username,password) values(?,?)";
                     var data = [username,password];
                     db.run(sql_insert,data,err =>{
                        if (err) {
                            console.log("Error \n", err);
                            res.jsonp({
                                success:false,
                                error:err
                            })
                            }else{
                               res.jsonp({
                                   success:true,
                                   message:'Signed in succesfully'
                               })   
                            } 
                     })
                  }
              }
        })
    }

}

exports.login = (req,res) =>{
 
    const {username,password} = req.body;

     if(username =='' || password==''){
          res.jsonp({
              success:false,
              message:"Username and Password should not be empty"
          })
     }else{
        var data=[username,password];
        var sql_select = "select * from user where username=(?) AND password=(?)";
        db.all(sql_select,data, (err,rows) =>{
            if (err) {
                console.log("Error \n", err);
                        res.jsonp({
                            success:false,
                            error:err
                        })
              }else{
                 if(rows.length > 0){
                    res.jsonp({
                        success:true,
                        message:'Login successful'
                    })
                 }else{
                    res.jsonp({
                        success:false,
                        message:'Username or Password is incorrect'
                    })
                 }
              }
        })
     }

}