const db = require('../db');
const fs = require('fs');
const csvStringify = require('csv-stringify');
var multer = require('multer');



exports.createUser = (req,res) =>{

 const { name,email,mobile_no,gender,photo_url} = req.body;
 if(name ==''||email==''||mobile_no==''||gender==''){

  res.jsonp({
    success:false,
    message:"All Fileds Required"
})
 }else{

  const user = [name,email,mobile_no,gender,photo_url]; 

  const sql_insert = `INSERT INTO users_details (name, email, mobile_no, gender, photo_url) VALUES (?,?,?,?,?)`;
     db.run(sql_insert,user, err => {
         if (err) {
         console.log("Error \n", err);
         res.jsonp({
             success:false,
             error:err
         })
         }else{
            res.jsonp({
                success:true,
                message:'Records inserted successfully'
            })   
         } 
         });
 }

   
}

exports.getUsers = (req,res) =>{
  
  const sql_select = `select * from users_details`;

  db.all(sql_select, [], (err, rows) => {

    if (err) {
      console.log("Error \n", err);
      res.jsonp({
          success:false,
          error:err
      })
    }
    res.jsonp({
        success:true,
        result: rows
    })
  });
}

exports.csvGenerator = (req,res) =>{
  const sql_select = `select * from users_details`;

  db.all(sql_select, [],async (err, rows) => {

    if (err) {
      console.log("Error \n", err);
      res.jsonp({
          success:false,
          error:err
      })
    }
      await createCSV(rows).then(response =>{
        res.download('report.csv', (err) =>{
          if(err){
            console.log("Error \n", err);
            res.jsonp({
                success:false,
                error:err
            })
          }else{
            fs.unlink(response.filename, (err) => {
              if (err){
                console.log("Error \n", err);
                res.jsonp({
                    success:false,
                    error:err
                })
              }
            });
          }
        })
      })
  });

}

exports.UploadImage=(req,res)=>{
  var Storage = multer.diskStorage({
      destination: function (req, file, callback) {
          callback(null, "./src");
      },
      filename: function (req, file, callback) {
         // callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
         callback(null,file.originalname);
      }
  });
  
  var upload = multer({ storage: Storage }).array("File", 3); //Field name and max count
  
  upload(req, res, function (err) {
      if (err) {  
          return res.end("Something went wrong!");
      }
      return res.end("File uploaded sucessfully!.");
  });
  
  }
  

function createCSV(data){
  return new Promise((resolve,reject) =>{
    var filename = 'report.csv'
    var columns = {
     id:'id',
     name:'Name',
     email:'Email',
     mobile_no:'Mobile_no',
     gender:'Gender',
     photo_url:'Photo_url',
    };
  
    csvStringify(data, {header: true,columns: columns}, (err, output) => {
    
      if (err) {
        console.log("ERROR 🚨\n",err);
        resolve({
          success: false
        })
      }else{
  
        fs.writeFile(filename, output, async function (err) {
          if (err) {
            console.log('ERROR 🚨\n',err)
            resolve({
              success: false
            })
          } else {
            // console.log('CSV FILE CREATED ✔️');
            resolve({
              success:true,
              filename:filename
            })
          }
        });
  
      }
    })
  })
}