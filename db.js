const sqlite3 = require('sqlite3').verbose();

let db = new sqlite3.Database('./DB/task.db',sqlite3.OPEN_READWRITE, (err) => {
  if (err) {
    return console.error(err.message);
  }
  console.log('CONNECTED TO SQLOTE DATABASE');
});

// const sql_create = `CREATE TABLE IF NOT EXISTS users_details (
//     id INTEGER PRIMARY KEY AUTOINCREMENT,
//     name VARCHAR(100) NOT NULL,
//     email VARCHAR(100) NOT NULL,
//     mobile_no VARCHAR(100) NOT NULL,
//     gender VARCHAR(20) NOT NULL,
//     photo_url VARCHAR(500) NOT NULL
//   );`;

// const sql_create = `CREATE TABLE IF NOT EXISTS user (
//     id INTEGER PRIMARY KEY AUTOINCREMENT,
//     username VARCHAR(100) NOT NULL,
//     password VARCHAR(100) NOT NULL
//   );`;

// db.run(sql_create, err => {
//   if (err) {
//     return console.error(err.message);
//   }
//   console.log("TABLE CREATED SUCCESSFULLY");
// });

module.exports = db;