const express = require('express');
const app = express();
const port = 8000;
const db = require('./db');
const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require('morgan')
const fs = require("fs");
const dir = "./src";
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));
app.use(cors());
app.use(morgan("dev"));

if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

//routes
app.use('/', require('./routes/routes'));


app.listen(port,async () =>{
    console.log(`SERVER IS RUNNING ON PORT: ${port}`);
})


