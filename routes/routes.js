const router = require("express").Router();
const UserController = require('../controllers/user.controller');
const LoginController = require('../controllers/login.controller');

//-------------- Login Routes --------------//
router.post('/signin', LoginController.signin);
router.post('/login', LoginController.login);

//-------------- User Detail Routes --------------//
router.post('/createUser', UserController.createUser)
router.get('/getUser', UserController.getUsers);
router.get('/getCSV', UserController.csvGenerator);
router.post('/upload', UserController.UploadImage);


module.exports = router;